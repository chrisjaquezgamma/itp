<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 22/04/19
 * Time: 04:52 PM
 */

namespace Gamma\PokeAPI\Api\Data;


interface PokemonInterface
{
    const TYPE = 'type';
    const NAME = 'name';
    const MOVES = 'moves';
    const IMAGE = 'image';
    const REGION = 'region';
    const FLAVOR_TEXT = 'description';

    public function getType(): string;

    public function setType(string $type): PokemonInterface;

    public function getName(): string;

    public function setName(string $name): PokemonInterface;

    public function getMoves(): array;

    public function setMoves(array $moves): PokemonInterface;

    public function getImage(): string;

    public function setImage(string $image): PokemonInterface;

    public function getRegion(): string;

    public function setRegion(string $region): PokemonInterface;

    public function getFlavorText(): string;

    public function setFlavorText(string $flavorText): PokemonInterface;
}